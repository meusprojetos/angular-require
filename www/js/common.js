define(['angularAMD',
        'angular',
        'ui.router',
    ],
    function (angularAMD) {
        'use strict';
        return angularAMD;
    });
