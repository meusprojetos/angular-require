/**
 * Created by Alan on 19/07/2015.
 */

define(['common','ngMaterial'], function (angularAMD) {
    'use strict';
    var app = angular.module('app', [
        'ui.router',
        'ngMaterial',

    ]);

    app.config(['$stateProvider', '$urlRouterProvider',
        function ($stateProvider, $urlRouterProvider) {

            $stateProvider
                .state('teste', angularAMD.route({
                    url: '/teste',
                    templateUrl: 'views/teste.html',
                    controllerUrl: 'controller/TesteCtrl'
                }))
                .state('home', angularAMD.route({
                    url: '/home',
                    templateUrl: 'views/home.html',
                    controllerUrl: 'controller/HomeCtrl'
                }))
                .state('login', angularAMD.route({
                    url: '/login',
                    templateUrl: 'views/login.html',
                    controllerUrl: 'controller/LoginCtrl'
                }))
                .state('users', angularAMD.route({
                    url: '/users',
                    templateUrl: 'views/user.html',
                    controllerUrl: 'controller/UserCtrl'
                }))
            ;

        $urlRouterProvider
            .otherwise('/teste');
    }]);

    return angularAMD.bootstrap(app);
});
