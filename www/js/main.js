/**
 * Created by Alan on 19/07/2015.
 */
require.config({
    baseUrl: 'js/',

    paths: {
        'angular':      '../bower_components/angular/angular',
        'ngMaterial':   '../bower_components/angular-material/angular-material',
        'ngAnimate':    '../bower_components/angular-animate/angular-animate',
        'ngAria':       '../bower_components/angular-aria/angular-aria',
        'ui.router':    '../bower_components/angular-ui-router/release/angular-ui-router',
        'angularAMD':   '../bower_components/angularAMD/angularAMD',
        'ngload':       '../bower_components/angularAMD/ngload'

    },

    shim: {
        'angularAMD': ['angular' ],
        'ngAria':['angular'],
        'ngAnimate':['angular'],
        'ngload': ['angularAMD' ],
        'ui.router': ['angular'],
        'ngMaterial':['angular','ngAria','ngAnimate']
    },

    deps: ['app']
});
