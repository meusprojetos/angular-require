/**
 * Created by Alan on 19/07/2015.
 */

define(['app'], function () {
    'use strict';
    return ['$scope', function ($scope) {
        $scope.message = 'User page';
    }];
});
